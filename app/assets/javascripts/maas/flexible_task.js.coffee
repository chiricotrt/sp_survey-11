## MAIN CALL
jQuery ->
  # Load tooltips
  $(document).tooltip()

  # On selection of mode-features,
  # - add to plan box
  # - calculate price and display
  $(".mode_feature").click ->
    addToPlan($(this))

    $(".switch-input").prop("disabled", !modesCheck())

  # On selection of "I don't want to use the service", show the reasons
  $("input[name='buy_chosen_plan']").click ->
    if $("#buy_chosen_plan_2").prop("checked")
      $("#reasons_for_not_buying").show()
    else
      $("input[name='reasons_for_not_buying[]']").attr('checked',false);
      $("#reasons_for_not_buying").hide()

  # On submit of form
  $('#flexible_task').on 'submit', () ->
    ## Perform validations ##
    # minimum number of modes
    if !modesCheck()
      console.log "minimum_mode error"
      $("#minimum_mode_error").show().delay(5000).fadeOut()
      $(window).scrollTop($('#minimum_mode_error').offset().top)
      return false

    # buy_chosen_plan
    if $("input[name='buy_chosen_plan']").length > 0 and not $("input[name='buy_chosen_plan']:radio:checked").val()
      $("#buy_chosen_plan_error").show().delay(5000).fadeOut()
      $(window).scrollTop($('#buy_chosen_plan_error').offset().top)
      return false

    # if reasons_for_not_buying not selected (only if buy_chosen_plan = 3)
    if $("#buy_chosen_plan_2").prop("checked") and $("input[name='[]']:checked").length == 0
      $("#reasons_for_not_buying_error").show().delay(5000).fadeOut()
      $(window).scrollTop($('#reasons_for_not_buying_error').offset().top)
      return false

  # DESIGN 2 #
  $(".switch-input").click ->
    if $(this).prop("checked")
      # Show choice_question
      $("#choice_question").show()

      # Disable changing of mode-features
      toggleVisibility($("ul.main_modes"), false)
    else
      $("#choice_question").hide()

      # Enable changing of mode-features
      toggleVisibility($("ul.main_modes"), true)

  $('#flexible_task_2').on 'submit', () ->
    ## Perform validations ##
    # minimum number of modes
    if !modesCheck()
      $("#minimum_mode_error").show().delay(5000).fadeOut()
      $(window).scrollTop($('#minimum_mode_error').offset().top)
      return false

    # happy with plan
    if !$(".switch-input").prop("checked")
      $("#happy_with_plan_error").show().delay(5000).fadeOut()
      $(window).scrollTop($('#happy_with_plan_error').offset().top)
      return false

    # buy_chosen_plan
    if $("input[name='buy_chosen_plan']").length > 0 and not $("input[name='buy_chosen_plan']:radio:checked").val()
      $("#buy_chosen_plan_error").show().delay(5000).fadeOut()
      $(window).scrollTop($('#buy_chosen_plan_error').offset().top)
      return false

    # if proposed_amount not selected (only if buy_chosen_plan = 2)
    if $("#buy_chosen_plan_2").prop("checked") and $("#proposed_amount").val() <= 0
      $("#proposed_amount_error").show().delay(5000).fadeOut()
      $(window).scrollTop($('#proposed_amount_error').offset().top)
      return false

    # Re-enable modes before submit
    toggleVisibility($("ul.main_modes"), true)

toggleVisibility = (element, status) ->
  element.find("input[type='radio']").each ->
    $(this).attr("disabled", !status)

addToPlan = (element) ->
  # NOTE: If "None" is selected, remove from plan
  if element.val() == "0"
    removeFromPlan(element)
  else
    icon = element.data("icon")
    $("#li_#{icon}").show()
    $("#span_#{icon}").text(element.data("label"))
    $("#span_#{icon}").attr("data-price", element.data("price"))
    updatePrice()

removeFromPlan = (element) ->
  icon = element.data("icon")
  $("#li_#{icon}").hide()
  $("#span_#{icon}").text("")
  $("#span_#{icon}").attr("data-price", 0)
  updatePrice()

updatePrice = ->
  # Iterate over price values in plan box and calculate total price
  total_price = 0
  $("span.mode_feature_span").each ->
    total_price += parseFloat($(this).attr("data-price"))

  # Set form field
  $("#flexible_plan_price").val(total_price)
  total_price = Math.round(total_price)

  # Split into integer and decimal
  price_decimal = total_price % 1
  price_integer = total_price - price_decimal

  # Update display
  $("#plan_price_integer").html(price_integer)
  if price_decimal == 0
    $("#plan_price_fraction").html("00")
  else
    $("#plan_price_fraction").html(price_decimal.toFixed(2) * 100)

modesCheck = ->
  selected_modes = [
    $("input[name='mode_features[public_transport]']:radio:checked").val(),
    $("input[name='mode_features[bike_sharing]']:radio:checked").val(),
    $("input[name='mode_features[taxi]']:radio:checked").val(),
    $("input[name='mode_features[car_sharing]']:radio:checked").val()
  ]
  non_null_array = selected_modes.filter (value) -> (value and value != "0")
  non_null_array.length >= 1