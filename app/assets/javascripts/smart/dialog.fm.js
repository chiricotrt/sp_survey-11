(function( $ ) {
	var dialogMethods = {

		"dialog.confirm": function(callback){
			console.log("called dialog.create");
			var that = this;
			if (!this.hasClass('fmDialog')){
				this.addClass('fmDialog');
				buttons = $('<div class="dialog_control"><a class="btn yes">Yes</a><a class="btn no">Cancel</a></div>');
				this.append(buttons);
				$('a.no', this).unbind('click');
				$('a.yes', this).unbind('click');
			}
			$('a.no', this).click(function() { callback(this, 'no');  that.hide(); });
			$('a.yes', this).click(function() { callback(this, 'yes'); that.hide(); } );
			this.show();
		},

		"dialog.show": function(){
			this.show();
		},

		"dialog.hide": function(){
			this.hide();
		}
	};

	jQuery.fn.fm('loadModule', dialogMethods);

})( jQuery );