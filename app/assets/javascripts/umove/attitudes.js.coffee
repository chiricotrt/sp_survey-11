## MAIN CALL
jQuery ->
  $('#sp_attitudes_resident').on 'submit', () ->
    if $("input[name^=attitude_towards_maas]:checked").length.toString() != $("#attitude_towards_maas_count").val()
      $("#attitude_towards_maas_error").show().delay(5000).fadeOut()
      $(window).scrollTop($('#attitude_towards_maas_error').offset().top)
      return false

    if $("input[name^=attitude_towards_car]:checked").length.toString() != $("#attitude_towards_car_count").val()
      $("#attitude_towards_car_error").show().delay(5000).fadeOut()
      $(window).scrollTop($('#attitude_towards_car_error').offset().top)
      return false

    if $("input[name^=modes_affected]:checked").length.toString() != $("#modes_affected_count").val()
      $("#modes_affected_error").show().delay(5000).fadeOut()
      $(window).scrollTop($('#modes_affected_error').offset().top)
      return false