class City < ActiveRecord::Base
  attr_accessible :name, :lat, :lon

  has_many :users

  # Ids for cities
  OXFORDSHIRE = 1
  TURIN = 2
  BUDAPEST = 3
  AACHEN = 4

  def self.city_from_country(country_code)
    country_code = 'GB' if country_code.blank?
    case country_code.upcase
    when "DE", "AACHEN"
      AACHEN
    when "IT", "TURIN", "3"
      TURIN
    when "GB", "OXFORDSHIRE", "4"
      OXFORDSHIRE
    when "HU", "BUDAPEST", "2"
      BUDAPEST
    else # default
      Rails.logger.info "Invalid city!!!"
      OXFORDSHIRE
    end
  end

  def self.id_from_name(name)
    const_get(name.upcase)
  end

  def currency
    case id
    when OXFORDSHIRE
      "£"
    when "BUDAPEST"
      "Ft"
    when TURIN
      "€"
    end
  end

  def budapest?
    (id == BUDAPEST)
  end

  def export_tourist_prefix
    case id
    when OXFORDSHIRE
      11000
    when BUDAPEST
      22000
    end
  end

  def export_resident_prefix
    case id
    when OXFORDSHIRE
      1000
    when BUDAPEST
      2000
    when TURIN
      3000
    end
  end

  def export_dataset_value
    case id
    when OXFORDSHIRE
      1
    when BUDAPEST
      2
    when TURIN
      3
    end
  end

end
