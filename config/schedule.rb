# # Learn more: http://github.com/javan/whenever

# job_type :command, ":task :output"
# job_type :rake,    "cd :path && :environment_variable=:environment bundle exec rake :task --silent :output"
# job_type :runner,  "cd :path && RAILS_ENV=:environment bundle exec script/runner ':task' :output"
# job_type :script,  "cd :path && :environment_variable=:environment bundle exec script/:task :output"
# job_type :runner3,  "cd :path && bin/rails runner -e :environment ':task' :output"

# set :user, ENV['USER_CAPISTRANO']
# set :application, ENV['APPLICATION']
# set :output, "#{path}/log/cron.log"

# every 10.minutes, :roles => [:slave] do
# 	# runner3 "NotificationVerifier.mobileAppOffVerifier"
# 	#runner "# -- #{:stage}"
# end

# every 5.minutes, :roles => [:master] do
#   runner3 "RawDataConsumerService.execute"
# end

# every 1.day, :at => "3:00am", :roles => [:slave] do
#   runner3 "/home/#{user}/#{application}/current/jobs/createFrequentPlaceAndSignatures.rb"
#   runner3 "/home/#{user}/#{application}/current/jobs/stopStatistics.rb"
#   runner3 "AlarmManager.checkUserStops"
#   runner3 "AlarmManager.checkUserBattery"
# end

# every 1.day, :at => "5:00am", :roles => [:master] do
#   command "/bin/sh /home/#{user}/#{application}/current/script/uploadMaintenance.sh #{user}"
# end

# #every 1.day, :at => "0:00am", :roles => [:slave] do
#   #runner "NotificationVerifier.startVerificationProcess"
# #end

# every 12.hours, :roles => [:slave] do
#   runner3 "UpdateUserStats.whichUsersToUpdate"
# end


# every 1.day, :at => "1:00am", :roles => [:slave] do
#   runner3 "GeneralBatteryStats.calculateBattStatsPerOS"
# end

# every 1.day, :at => "1:30am", :roles => [:slave] do
#   runner3 "UpdateUserStats.updateUsersLoggedTime"
# end

# every 1.day, :at => "0:01am", :roles => [:master] do
#   runner3 "UserClassifier.check_active_users"

#   # check if this is still needed, doesn't appear to do anything without additional arguments
#   # runner3 "tools/generate_user_status_report.rb"
# end

# every 1.hour, :roles => [:master] do
#   # runner3 "PushNotificationSender.perform"
#   runner3 "LoggerStopDetectionService.execute" # Stop detection for logger data
#   rake "geocode:missing_stops"
# end

# # every 10.minutes, :roles => [:slave] do
# every '7,17,27,37,47,57 * * * *', :roles => [:slave] do
#   runner3 "ExternalApiData::Weather.update_latest"
#   runner3 "ExternalApiData::TubeLine.update_latest"
#   runner3 "ExternalApiData::BusLine.update_latest"
#   runner3 "ExternalApiData::NationalRailLine.update_latest"
#   runner3 "ExternalApiData::RoadSeverity.update_latest"
#   runner3 "ExternalApiData::BikePoint.update_latest"
#   runner3 "ExternalApiData::TflCarParkOccupancy.update_latest"
#   runner3 "ExternalApiData::WestministerCarParkOccupancy.update_latest"
#   runner3 "ExternalApiData::CamdenCarParkOccupancy.update_latest"
# end

# every 1.day, :at => "2:00am", :roles => [:slave] do
#   runner3 "ExternalApiData::RoadDisruption.update_latest"
#   runner3 "ExternalApiData::TflCarPark.update_latest"
#   runner3 "ExternalApiData::WestministerCarPark.update_latest"
#   runner3 "ExternalApiData::CamdenCarPark.update_latest"
# end
