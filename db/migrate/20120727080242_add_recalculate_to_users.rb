class AddRecalculateToUsers < ActiveRecord::Migration
  def self.up
    add_column :users, :recalculate, :boolean, :default => false
  end

  def self.down
    remove_column :users, :recalculate
  end
end
