class ModifyLocationsFromStringToText < ActiveRecord::Migration
  def change
    remove_column :pre_survey_socio_economic_characteristics, :home_location, :string
    remove_column :pre_survey_socio_economic_characteristics, :work_locations, :string
    remove_column :pre_survey_socio_economic_characteristics, :shop_locations, :string
    remove_column :pre_survey_socio_economic_characteristics, :leisure_locations, :string

    add_column :pre_survey_socio_economic_characteristics, :home_location, :text
    add_column :pre_survey_socio_economic_characteristics, :work_locations, :text
    add_column :pre_survey_socio_economic_characteristics, :shop_locations, :text
    add_column :pre_survey_socio_economic_characteristics, :leisure_locations, :text
  end
end
