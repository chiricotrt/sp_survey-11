class AddUserIdToPreSurveys < ActiveRecord::Migration
  def change
    add_column :pre_surveys, :user_id, :integer
  end
end
