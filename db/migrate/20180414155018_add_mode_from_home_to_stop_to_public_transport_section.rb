class AddModeFromHomeToStopToPublicTransportSection < ActiveRecord::Migration
  def change
    add_column :pre_survey_public_transport_characteristics, :mode_from_home_to_stop, :integer
  end
end
